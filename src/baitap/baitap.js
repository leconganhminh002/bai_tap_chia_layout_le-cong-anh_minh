import React from "react";
import Header from "./header";
import Carousel from "./carousel";
import SmartphoneList from "./smartphone";
import LaptopList from "./laptop";
import Promotion from "./promotion";

export default function Baitap() {
  return (
    <>
      <Header />
      <Carousel />
      <SmartphoneList />
      <LaptopList />
      <Promotion />
    </>
  );
}
